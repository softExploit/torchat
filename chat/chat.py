import socket
import socks
from queue import Queue
from threading import Thread
import os
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
from time import sleep
from subprocess import call
import readline

socks_port = 9050

#r32pimix7kooaotlrqasn67ku6mdjcn3uc7zls2nkqccckhwesiresqd.onion
#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s = socket.socksocket()
def tor():
    global s
    socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", socks_port, True)
    #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s = socks.socksocket()
    s.connect((addy, int(port)))
    
    
def clearnet():
    global s
    #socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", port, True)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #s = socket.socksocket()
    s.connect((addy, int(port)))

torme = input("Use Tor on %s? [y/n]: " % socks_port)
addy = input("Address: ")
port = input("Port: ")
if torme.lower() == "yes" or torme.lower() == "y":
    tor()
else:
    clearnet()
#host = 'sk6vrljjxpqzdfg3c5wogufbhvfwxe4flem5l6cba3z7wns2ts75fsid.onion'
#port = 5001
ThreadCount = 0
q = Queue()
#s.connect((host, port))
name = input("Enter Your Nick: ")
s.send(name.encode('utf-8'))
print('> Locked And Loaded.')
print('> Waiting For Connections...')
data = "" 


class BC:
    G = '\033[92m'
    A = '\033[0;37m'
    F = '\033[91m'
    B = '\033[1m'
    E = '\033[0m'
    
    
def tor():
    global s
    socks.set_default_proxy(socks.SOCKS5, "127.0.0.1",SOCKS_PORT)
    socket.socket = socks.socksocket
    s = socket.socket()
    #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', port))
    s.listen(500)

def clearnet():
    global s
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', port))
    s.listen(500)

        
def encrypt(msg):
    global encrypted
    encryptor = PKCS1_OAEP.new(pubKey)
    encrypted = encryptor.encrypt(msg)
    encrypt = binascii.hexlify(encrypted)
    encrypted = encrypt.decode()
            

def decrypt(mssg):
    global decrypted
    try:
        msg = binascii.unhexlify(mssg)
        decryptor = PKCS1_OAEP.new(privKeyPEM)
        decrypted = decryptor.decrypt(msg)
        decrypted = decrypted.decode()
    except ValueError:
        pass



def start():
    global mykey
    global prkey
    global pkey
    global pubKey
    global privKeyPEM
    global names
    global wel
    global dv
    global ex
    ex = 0
    dv = ""
    os.system('touch public.pem')
    os.system('touch private.pem')
    os.system('touch privated.pem')
    with open('public.pem') as f:
        pkey = f.read()
    if pkey == "":
        pass
    else:
        pubKey = RSA.importKey(pkey)
        #pubKey = int(pubKey)
    with open('privated.pem') as f:
        mykey = f.read()
    with open('private.pem') as f:
        prkey = f.read()
    if prkey == "":
        pass
    try:
        privKeyPEM = RSA.importKey(prkey)
        #pubKey = int(pubKey)
        print(privKeyPEM)
    except ValueError:
        pass
    initing()


def sending(jj):
    #dat = q.get()
    encrypt(jj.encode('utf-8'))
    s.send(encrypted.encode('utf-8'))
    #q.queue.clear()
            
            
        
def threaded_user(connection):
    global data
    global enter
    #name = connection.recv(2048).decode('utf-8')
    enter = BC.F + name + BC.E + " has connected."
    sending(enter)
    while True:
        data = connection.recv(2048)
        check = data.decode('utf-8')
        if check[:4] == "User":
            print(check)
        elif check[:1] == "@":
            print(check)
        elif check[:7] == "Current":
            print(check)
        elif check[:8] == "Password":
            ms = input("Enter Password: ")
            ms = "Password " + ms
            s.send(ms.encode('utf-8'))
        elif check[:6] == "Admins":
            print(check)
        elif check[:5] == "Error":
            print(check)
            print("Connection Terminated.")
        elif check == "You have been kicked!":
            print(ms)
            s.close()
            os.system('pkill chat.py')
        else:
            try:
                decrypt(data.decode('utf-8'))
                #reply = name + ': ' + data.decode('utf-8')
                q.put(decrypted)
                print(decrypted)
            except NameError:
                pass



def initing():
    Thread(target=threaded_user, args=(s,)).start()
    s.send(name.encode('utf-8'))
    while True:
        global ms
        q.queue.clear()
        ms = input("")
        if len(ms) >= 240:
            print(BC.F + "Your message must be below 200 characters. Not sent." + BC.E)
        elif ms.lower() == "exit":
            s.send(ms.encode('utf-8'))
            s.close()
            os.system('pkill chat.py')
        elif ms.lower() == "!list":
            s.send(ms.encode('utf-8'))
        elif ms[:4].lower() == "!add":
            s.send(ms.encode('utf-8'))
        elif ms[:7].lower() == "!admins":
            s.send(ms.encode('utf-8'))
        elif ms[:5].lower() == "!kick":
            s.send(ms.encode('utf-8'))
        elif ms[:7].lower() == "!remove":
            s.send(ms.encode('utf-8'))
        elif ms[:1] == "@":
            ms = ms + " [From " + name + "]\n"
            s.send(ms.encode('utf-8'))
        else:
            #q.put(ms)
            ms = BC.G + name + BC.E + ": " + ms
            sending(ms)
            
    ServerSocket.close()

start()