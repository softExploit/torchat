# TorChat by Holistic Coding
> Anonymous/Sensitive Chatting

TorChat is an end-to-end RSA 3072 encrypted chat client that can (optionally) be hosted and used over tor.
If you don't want to use tor just use ngrok and your clients may still use tor for their connections.

You may also re-generate your keys with rsa.py in the main directory.
If you do re-generate you must replace all .pem files in each directory and share the keys with other users.


Second Tool:
TorP2P SSH style - Currently under construction.
TorP2P Will have all the functionality that TorChat does, but for two users to access each others machines for maintainence and such.
This comes in handy when you want to keep your anonimity, but need a hand on your machine.
All commands will be requested by TorP2P prior to executing on your machine, so no malicious commands can be executed without consent.
It will also block commands such as 'curl ifconfig.me' locally to ensure your anonimity isn't broken. 


## Installation

Run the following in the torp2p directory. 
```
sudo pip3 install -r requirements.txt
cd chat
python3 server.py
```

For clients:
```
sudo pip3 install -r requirements.txt
cd chat
python3 chat.py
```


> It is recommended to host through ngrok, or forward ports and host through a tor onion.

~~~
 Directories: - [ ] Chat
                    - [ ] Server.py - Host/Server
                    - [ ] chat.py - For Clients
                    - [ ] privated.pem - key
                    - [ ] private.pem - key (null, will be used for exchange)
                    - [ ] public.pem - key
                -  [ ] p2p
                    - [ ] Chat_Main.py - Host/Server
                    - [ ] client.py - Pretty Self Explanatory.
                    - [ ] privated.pem - key
                    - [ ] private.pem - key (null, will be used for exchange)
                    - [ ] public.pem - key
                -  [ ] rsa.py - re-generate keys here, just be sure to place them in the directories.
                -  [ ] logo.py - Styling for rsa.py
                -  [ ] Requirements.txt - python requirements
More Coming Soon!
      Recomendations Welcomed
~~~




Tor P2P And Chat Server / Client.