
import sys
import os
import socket
import socks
import multiprocessing
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
from time import sleep
from subprocess import call

port = 5001 #Adjust To Your Liking.
SOCKS_PORT = 9050 #Change If Your Tor Does Not Use 9050.
print("Updating...")
sleep(1)
os.popen('git pull origin master')
sleep(2)


class BC:
    G = '\033[92m'
    A = '\033[0;37m'
    F = '\033[91m'
    B = '\033[1m'
    E = '\033[0m'
    
    
def tor():
    global s
    socks.set_default_proxy(socks.SOCKS5, "127.0.0.1",SOCKS_PORT)
    socket.socket = socks.socksocket
    s = socket.socket()
    #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', port))
    s.listen(500)

def clearnet():
    global s
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('0.0.0.0', port))
    s.listen(500)

def con():
    global clientsocket
    global address
    global wel
    while True:
        cli, address = s.accept()
        clientsocket.append(cli)
        print(address)
        print(clientsocket)
        wel = "Welcome!"
        for i in clientsocket:
            i.send(wel.encode('utf-8'))
        if ex == 1:
            sys.exit()
        b.communicate()

        
def encrypt(msg):
    global encrypted
    encryptor = PKCS1_OAEP.new(pubKey)
    encrypted = encryptor.encrypt(msg)
    encrypt = binascii.hexlify(encrypted)
    encrypted = encrypt.decode()
            

def decrypt(mssg):
    global decrypted
    msg = binascii.unhexlify(mssg)
    decryptor = PKCS1_OAEP.new(privKeyPEM)
    decrypted = decryptor.decrypt(msg)
    decrypted = decrypted.decode()



def start():
    global mykey
    global prkey
    global pkey
    global pubKey
    global privKeyPEM
    global names
    global wel
    global dv
    global b
    global ex
    ex = 0
    dv = ""
    os.system('touch public.pem')
    os.system('touch private.pem')
    os.system('touch privated.pem')
    with open('public.pem') as f:
        pkey = f.read()
    if pkey == "":
        pass
    else:
        pubKey = RSA.importKey(pkey)
        #pubKey = int(pubKey)
    with open('privated.pem') as f:
        mykey = f.read()
    with open('private.pem') as f:
        prkey = f.read()
    if prkey == "":
        pass
    try:
        privKeyPEM = RSA.importKey(prkey)
        #pubKey = int(pubKey)
        print(privKeyPEM)
    except ValueError:
        dv = "0"
    print(BC.E + "NOTE: You must forward %s and edit torrc accordingly to use an onion." % port)
    print("Listener Port: %s" % port)
    print("")
    torme = input("Use Tor On Port %s [y/n]: " % SOCKS_PORT)
    
    if torme.lower() == "yes" or torme.lower() == "y":
        tor()
    else:
        clearnet()
    names = input("Enter Your Username: ")
    wel = BC.G + names + BC.E + " has initiated the chat."
    print("")
    print("Waiting For Connection...")
    b = multiprocessing.Process(target=con)
    b.start()
    connect()



def get():
    global ex
    while True:
        try:
            for i in clientsocket:
                ab = i.recv(9999)
                ab = ab.decode('utf-8')
                decrypt(ab)
                print(decrypted)
            if ex == 1:
                sys.exit()
        except ValueError:
            print(BC.G + named + BC.E + " left.")
            ex = 1
            sys.exit()

def recieve():
    global ex
    global names
    multiprocessing.Process(target=get).start()
    while True:
        print("Message")
        sys.stdout.write("\033[F")
        ab = input("")
        sys.stdout.write("\033[F")
        print(BC.F + names + ": " + BC.E + ab)
        if ab[:6].lower() == "/nick ":
            changed = BC.G + names + BC.E + " Changed Their Nick To " + BC.G + ab[6:] + BC.E
            encrypt(changed.encode('utf-8'))
            names = ab[6:]
            sys.stdout.write("\033[F")
            print("Your nick is now " + BC.F + names + BC.E)
            for i in clientsocket:
                i.send(encrypted.encode('utf-8'))
        #if ab == "":
        #    get()
        #else:
        else:
            ab = BC.G + names + ': ' + BC.E + ab
            ab = ab.encode('utf-8')
            encrypt(ab)
            for i in clientsocket:
                i.send(encrypted.encode('utf-8'))
    #message()

clientsocket = []
def connect():
    global clientsocket
    global address
    global named
    global names
    global ex
    global b
    while True:
        clientsocketz, address = s.accept()
        clientsocket.append(clientsocketz)
        print("Connection From %s Has Been Established." % (str(address)))
        for i in clientsocket:
            i.send(wel.encode('utf-8'))
            i.send(names.encode('utf-8'))
        for i in clientsocket:
            named = i.recv(1024)
            decrypt(named.decode('utf-8'))
            #named = decrypted
            named = decrypted[:15]
            print(named)
        #rec = Thread(target=recieve)
        #rec.start()
        #ab = clientsocket.recv(1024)
        #print(ab.decode('utf-8'))
        if ex == 1:
            sys.exit()
        break
    recieve()
        
start()